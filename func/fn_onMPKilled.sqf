params ["_killType","_victim","_killer","_killingPlayer","_killerpos"];


if (({alive _x} count (units group _victim)) < 1 && !((getPlayerUID _victim) in TKBR_adminList) && !TKBR_debugMode) then
{
	format ["#kick %1", getPlayerUID _victim] call ExileServer_system_rcon_event_sendCommand;
};
