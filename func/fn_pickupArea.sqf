/*
	TOKYO EXILE Battle Royale
	pickupArea
*/


switch (toLower worldName) do
{
	case "altis":										// [16000,16000] w/ radius of 16000 works well for Altis
	{
		TKBR_MapCenterPos 	= [16000,16000];
		TKBR_MapRadius 		= 16000;
	};
	case "bornholm":									// Thanks to thirdhero for testing this info
	{
		TKBR_MapCenterPos 	= [11265,11265];
		TKBR_MapRadius 		= 12000;
	};
	case "esseker":										// Thanks to Flowrider for this info
	{
		TKBR_MapCenterPos 	= [6275,6350];
		TKBR_MapRadius 		= 5000;
	};
	case "taviana";										// Thanks to JamieKG for this info
	case "tavi":
	{
		TKBR_MapCenterPos 	= [12800,12800];
		TKBR_MapRadius 		= 12800;
	};
	case "namalsk":
	{
		TKBR_MapCenterPos 	= [6000,4000];
		TKBR_MapRadius 		= 6000;
	};
	default 											// Use "worldSize" to determine map center/radius (not always very nice).
	{
		private "_middle";
		_middle = worldSize/2;
		TKBR_MapCenterPos 	= [_middle,_middle];
		TKBR_MapRadius 		= _middle;
	};
};

TKBR_BrAreaPoint = [
	TKBR_MapCenterPos,
	0,
	TKBR_MapRadius,
	0,
	0,
	0.9,
	0
] call BIS_fnc_findSafePos;
"BR Area set" call TKBR_fnc_util_log;
TKBR_BrBluezonePoint = [
	TKBR_BrAreaPoint,
	0,
	(TKBR_gameAreaRadius - TKBR_blueZoneSize),
	0,
	0,
	0.9,
	0
] call BIS_fnc_findSafePos;
"First BZ Set" call TKBR_fnc_util_log;
TKBR_BrAreaMarker = createMarker ["TKBR_BattleArea", TKBR_BrAreaPoint];
TKBR_BrAreaMarker setMarkerShape "ELLIPSE";
TKBR_BrAreaMarker setMarkerBrush "Border";
TKBR_BrAreaMarker setMarkerSize [TKBR_gameAreaRadius,TKBR_gameAreaRadius];
TKBR_BrAreaMarker setMarkerColor "ColorBlack";
publicVariable "TKBR_BrAreaMarker";

