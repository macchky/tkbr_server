private ["_messageRemain","_displayTime","_playerRemains","_currentPlayerCount"];

_displayTime = 5;
_messageRemain = "";
while {true} do {
	_currentPlayerCount = TKBR_playersRemaining;
	waitUntil { _currentPlayerCount != TKBR_playersRemaining};
	sleep 2;
	if (TKBR_playersRemaining <= 1) exitWith {};

	_messageRemain = format["%1 DEAD, %2 TO GO!",TKBR_playersInitial - TKBR_playersRemaining, TKBR_playersRemaining];
	[1,_messageRemain,_displayTime] remoteExec ["TKBRClient_showMessage", -2];
};

true