private ["_message","_displayTime"];

_message = "";
_displayTime = 5;

while {!TKBR_minimumReached} do {
	_message = format["WAITING %1 PLAYER TO START ROUND",(TKBR_minimumPlayers - ({alive _x} count allPlayers))];
	[1,_message,_displayTime] remoteExec ["TKBRClient_showMessage", -2];
	sleep 20;
	/*_message = format["MINIMUM PLAYER REACHED"];
	[1,_message,_displayTime] remoteExec ["TKBRClient_showMessage", -2];*/
};