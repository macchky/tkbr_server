params ["_zoneCenter","_zoneSize","_steps","_radStep","_data"];

_steps = floor ((2 * pi * _zoneSize) / 15);
_radStep = 360 / _steps;
_data = [];
for [{_j = 0}, {_j < 360}, {_j = _j + _radStep}] do {
	_pos2 = (_zoneCenter getPos [_zoneSize, _j]);
	_pos2 set [2, 0];
	_data set[count(_data),["UserTexture10m_F",_pos2,_j,"#(argb,8,8,3)color(0,0,1,0.4)"]];
	_data set[count(_data),["UserTexture10m_F",_pos2,(_j + 180),"#(argb,8,8,3)color(0,0,1,0.4)"]];
};

_data spawn {
	_old = TKBR_ZoneObjects;
	TKBR_ZoneObjects = [];
	_data = _this;
	if (TKBR_debugMode) then {
		diag_log _data;
	};
	_textures = [];
	{
		_type = _x select 0;
		_position = _x select 1;
		_dir = _x select 2;
		_texture = _x select 3;
		_obj = _type createVehicle _position;
		_obj setDir _dir;
		_obj setPosATL _position;
		_obj enableSimulationGlobal false;
		_textures set [count(_textures),_texture];
		TKBR_ZoneObjects set [count(TKBR_ZoneObjects),_obj];
	} forEach _data;
	{
		_x setObjectTextureGlobal [0,_textures select _forEachIndex];
	} forEach TKBR_ZoneObjects;
	{
		deleteVehicle _x;
	} forEach _old;
};