/*
	File: startZone.sqf

*/

private ["_blzcount","_zoneChange","_firstchangeTime","_zoneCenter","_oldZoneCenter","_zoneSizeScaling","_didUpdateMap","_bluezone"];

waitUntil {TKBR_game_started && TKBR_playerDeployed};

TKBR_bluezoneCreated = false;
TKBR_bluezoneStarted = false;

_blzcount = 6;

_firstchangeTime = TKBR_freeRoamTime;
_changeTime = TKBR_blueZoneDelay;
_zoneCenter = TKBR_BrBluezonePoint;
_zoneSize = TKBR_blueZoneSize;
_oldZoneCenter = [0,0,0];
_zoneSizeScaling = 0.5;
_didUpdateMap = false;

_bluezone = "";

_wt = 0;
_changeTime = 0;
if (TKBR_debugMode) then {
	_firstchangeTime = (2*60);
	_changeTime = (2*60);
};

for "_i" from 1 to _blzcount do
{
	if(!TKBR_game_started) exitWith {};

	if (_i == 1 && !TKBR_debugMode) then {
		_wt = TKBR_freeRoamTime * 60;
		_changeTime = (10*60);
		Sleep _wt;
	} else {
		if (!TKBR_debugMode) then {
			_wt = TKBR_blueZoneDelay - (2*60);
			_changeTime = TKBR_blueZoneDelay - _wt;
			Sleep _wt;
		} else {
			_wt = (1*60);
			Sleep _wt;
		};
	};
	if (_i == 6) then {
		_zoneSizeScaling = 0.4;
	};

	format["No.%1 BZ Started",_i] call TKBR_fnc_util_log;
	_oldtime = time;

	while {(time - _oldtime) < _changeTime} do
	{
		if (_bluezone == "") then
		{
			_bluezone = createMarker ["Blue_Zone", _zoneCenter];
			_bluezone setMarkerColor "ColorBlue";
			_bluezone setMarkerShape "ELLIPSE";
			_bluezone setMarkerBrush "BORDER";
			_bluezone setMarkerSize [_zoneSize,_zoneSize];
			TKBR_BrBluezoneMarker = _bluezone;
			publicVariable "TKBR_BrBluezoneMarker";
			TKBR_bluezoneCreated = true;
			publicVariable "TKBR_bluezoneCreated";
			[_zoneCenter,_zoneSize] spawn TKBR_fnc_createBlueZoneTex;
			_oldZoneCenter = _zoneCenter;
			_didUpdateMap = true;
			TKBR_bluezoneStarted = false;
			publicVariable "TKBR_bluezoneStarted";
			[2, "YOUR MAP HAS BEEN UPDATED!",10] remoteExec ["TKBRClient_showMessage", -2];
			sleep 5;
		};
		if !(_didUpdateMap) then
		{
			_oldZoneCenter = _zoneCenter;
			_zoneCenter = [
				_zoneCenter,
				0,
				(_zoneSize * (1-_zoneSizeScaling)),
				0,
				0,
				0.9,
				0
			] call BIS_fnc_findSafePos;
			_zoneSize = (_zoneSize * _zoneSizeScaling);
			_bluezone setMarkerSize [_zoneSize,_zoneSize];
			_bluezone setMarkerPos [(_zoneCenter select 0),(_zoneCenter select 1)];
			[_zoneCenter,_zoneSize] spawn TKBR_fnc_createBlueZoneTex;
			_didUpdateMap = true;
			TKBR_bluezoneCreated = true;
			publicVariable "TKBR_bluezoneCreated";
			TKBR_bluezoneStarted = false;
			publicVariable "TKBR_bluezoneStarted";
			[2, "YOUR MAP HAS BEEN UPDATED!",10] remoteExec ["TKBRClient_showMessage", -2];
			sleep 5;
		} else {
			sleep 5;
		};
		_remainTime = ceil ((_changeTime - (time - _oldtime)) / 60);
		format["BZ %1 min reamain and %2 sec",_remainTime,(_changeTime - (time - _oldtime))] call TKBR_fnc_util_log;
		[2,format ["IN %1 MINUTES PLAY RESTRICTED TO INSIDE THE BLUE ZONE",(ceil ((_changeTime - (time - _oldtime)) / 60))],10] remoteExec ["TKBRClient_showMessage", -2];
		Sleep 55;
	};
	_didUpdateMap = false;
	TKBR_bluezoneCreated = false;
	publicVariable "TKBR_bluezoneCreated";
	TKBR_bluezoneStarted = true;
	publicVariable "TKBR_bluezoneStarted";
	[2,"PLAY IS NOW RESTRICTED TO THE AREA INSIDE THE BLUE ZONE!",10] remoteExec ["TKBRClient_showMessage", -2];
};
UrbanW_InGame = false;
["ROUND ENDED!"] call TKBR_fnc_util_log;