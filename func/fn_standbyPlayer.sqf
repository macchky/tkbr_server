/*
	Accepting players
*/
private ["_type","_message","_displayTime","_addtime"];

_type = 1;
_message = "";
_displayTime = 5;
_addtime = 90;

"standby Players" call TKBR_fnc_util_log;

waitUntil {
	uiSleep 5;
	(((({alive _x} count allPlayers) > TKBR_minimumPlayers && (!TKBR_testMode || !TKBR_debugMode))) || TKBR_forceStart);
};

"MINIMUM PLAYER REACHED" call TKBR_fnc_util_log;
_message = "MINIMUM PLAYER REACHED";
[1,_message,_displayTime] remoteExec ["TKBRClient_showMessage", -2];
missionNamespace setVariable ["TKBR_minimumReached", true, true];
uiSleep _displayTime;

"Wait for additional players" call TKBR_fnc_util_log;
_message = "ADDITIONAL PLAYER WAITING FOR 90 SECONDS";
[1,_message,_displayTime] remoteExec ["TKBRClient_showMessage", -2];
uiSleep _displayTime;

if (TKBR_forceStart) then {
	_addtime = 1;
};
uiSleep _addtime;

remoteExec ["TKBRClient_setSpectatorGroup", -2];
"#lock" call ExileServer_system_rcon_event_sendCommand;

missionNamespace setVariable ["TKBR_playersInitial", ({alive _x} count allPlayers), true];

"Loading PLayers" call TKBR_fnc_util_log;
_message = "LOADING PLAYERS";
[1,_message,_displayTime] remoteExec ["TKBRClient_showMessage", -2];
uiSleep _displayTime;

missionNamespace setVariable ["TKBR_game_started", true, true];

deleteMarker "SpawnSyrtaIcon";
deleteMarker "TraderZoneFolia";
deleteMarker "SpawnZoneBR";

0 remoteExec ["TKBRClient_cinemaEffect", -2];

0 spawn TKBR_fnc_preparePlane;
0 spawn TKBR_fnc_startBluezone;
0 spawn TKBR_fnc_killCounter;