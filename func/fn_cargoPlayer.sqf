private ["_allPlayers","_numPlayer","_numPlane"];

_allPlayers = allPlayers;
_numPlayer = count _allPlayers;
_numPlane = ceil (_numPlayer / 30);

{
	_player = _x;
	if (TKBR_debugMode || TKBR_testMode) then {
		format ["Client %1 moveined",_player] call TKBR_fnc_util_log
	};
	if (_forEachIndex < 30) then {
		_plane = TKBR_cargoPlane1;
		[_plane] remoteExecCall ["TKBRClient_moveInCargo", _player];
	};
	if (_forEachIndex > 29 && _forEachIndex < 60) then {
		_plane = TKBR_cargoPlane2;
		[_plane] remoteExecCall ["TKBRClient_moveInCargo", _player];
	};
	if (_forEachIndex > 59) then {
		_plane = TKBR_cargoPlane3;
		[_plane] remoteExecCall ["TKBRClient_moveInCargo", _player];
	};
	sleep 0.1;
} forEach allPlayers;

/*for "_i" from 3 to 32 do {
	[TKBR_cargoPlane1] remoteExec ["TKBRClient_moveInCargo", _i];
};
for "_i" from 33 to 62 do {
	[TKBR_cargoPlane2] remoteExec ["TKBRClient_moveInCargo", _i];
};
for "_i" from 63 to 92 do {
	[TKBR_cargoPlane3] remoteExec ["TKBRClient_moveInCargo", _i];
};*/
