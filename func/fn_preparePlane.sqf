/*
	preparePlane
*/

private["_flyHeight","_speed","_direction","_targetPosition","_distance","_endPosition","_planeClass","_i","_startPosition","_pilot","_plane","_waypoint","_loadPosition","_loadDistance","_numPlayer"];
_numPlayer = {((alive _x) && (str(side _x) != 'CIV'))} count allPlayers;
_group = createGroup independent;
"TKBR Plane - Spawning Plane..." call ExileServer_util_log;
_flyHeight = 1400;
_speed = 250;
_direction = random 360;
_targetPosition = [
	TKBR_BrAreaPoint,
	0,
	TKBR_gameAreaRadius - 1000,
	0,
	0,
	0.9,
	0
] call BIS_fnc_findSafePos;

missionNamespace setVariable ["TKBR_playersInitial", _numPlayer, true];
missionNamespace setVariable ["TKBR_playersRemaining", _numPlayer, true];

_targetPosition set [2, _flyHeight];
_distance = 2500;
_endPosition =
[
	(_targetPosition select 0) - (sin _direction) * _distance,
	(_targetPosition select 1) - (cos _direction) * _distance,
	_flyHeight
];
_planeClass = "B_T_VTOL_01_infantry_F";

_loadDistance = 2000;
_startPosition =
[
	(_targetPosition select 0) + (sin _direction) * _distance,
	(_targetPosition select 1) + (cos _direction) * _distance,
	_flyHeight
];

_loadPosition =
[
	(_targetPosition select 0) + (sin _direction) * _loadDistance,
	(_targetPosition select 1) + (cos _direction) * _loadDistance,
	_flyHeight
];

for "_i" from 1 to (ceil (_numPlayer / 30)) do
{
	_pilot1 = _group createUnit ["I_helicrew_F", _startPosition, [], 100, "PRIVATE"];
	_pilot1 setSkill 1;
	[_pilot1] joinSilent _group;
	_pilot2 = _group createUnit ["I_helicrew_F", _startPosition, [], 100, "PRIVATE"];
	_pilot2 setSkill 1;
	[_pilot2] joinSilent _group;
	_plane = createVehicle [_planeClass, _startPosition, [], 100, "FLY"];
	missionNamespace setVariable [format ["TKBR_cargoPlane%1", _i], _plane,true];
	clearBackpackCargoGlobal _plane;
	clearWeaponCargoGlobal _plane;
	clearMagazineCargoGlobal _plane;
	clearItemCargoGlobal _plane;
	_plane setVehicleAmmo 0;
	_plane setFuel 0.1;
	_pilot1 assignAsDriver _plane;
	_pilot1 moveInDriver _plane;
	_pilot1 allowDamage false;
	_pilot2 assignAsTurret [_plane, [0]];
	_pilot2 moveInTurret [_plane, [0]];
	_pilot2 allowDamage false;
	_plane allowDamage false;
	_plane flyInHeight _flyHeight;
	_plane disableAI "TARGET";
	_plane disableAI "AUTOTARGET";
	_direction = _direction + 180;
	_plane setDir _direction;
	_plane setVelocity [(sin _direction) * _speed, (cos _direction) * _speed, 0];
	_plane limitSpeed 300;
	_plane setPosATL _startPosition;
	_plane lock true;
	uiSleep 3;
};

_group allowFleeing 0;
_group setBehaviour "CARELESS";
_group setSpeedMode "FULL";
_group setFormation "WEDGE";
_group setCombatMode "BLUE";

_waypoint = _group addWaypoint [_loadPosition, 0];
_waypoint setWaypointType "MOVE";
_waypoint setWaypointCompletionRadius 200;
_waypoint setWaypointStatements ["true", "'Plane - Reached loading point...' call TKBR_fnc_util_log; 0 spawn TKBR_fnc_cargoPlayer;"];
_waypoint = _group addWaypoint [_targetPosition, 0];
_waypoint setWaypointType "MOVE";
_waypoint setWaypointCompletionRadius 200;
_waypoint setWaypointStatements ["true", "'Plane - Reached target point...' call TKBR_fnc_util_log; 0 spawn TKBR_fnc_ejectPlayer;"];
_waypoint = _group addWaypoint [_endPosition, 0];
_waypoint setWaypointType "MOVE";
_waypoint setWaypointCompletionRadius 200;
_waypoint setWaypointStatements ["true", "'Plane - Reached end...' call TKBR_fnc_util_log; {(vehicle _x) setDamage 1; _x setDamage 1; } forEach thisList;"];
