class CfgPatches
{
	class tkbr_server
	{
		requiredVersion=0.1;
		requiredAddons[]= {};
		units[]= {};
		weapons[]= {};
		magazines[]= {};
		ammo[]=	{};
	};
};

class CfgFunctions
{
	class tkbr
	{
		tag = "TKBR";
		class init
		{
			file = "tkbr_server\init";
			class startBR
			{
				postInit = 1;
			};
		};
		class main
		{
			file = "tkbr_server\func";
			class pickupArea {};
			class preparePlane {};
			class cargoPlayer {};
			class ejectPlayer {};
			class startBluezone {};
			class updateBluezone {};
			class standbyPlayer {};
			class forceStart {};
			class util_log {};
			class kickPlayer {};
			class createBlueZoneTex {};
			class restartMission {};
			class lobbyMessenger {};
			class fn_killCounter {};
		};
	};
};