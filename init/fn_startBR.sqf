/*
	TOKYO EXILE Battle Royale
	start BR !
*/

TKBR_game_started = false;
TKBR_playerDeployed = false;
TKBR_forceStart = false;
TKBR_alert = false;

TKBR_ZoneObjects = [];

TKBR_BrAreaPoint = [0,0,0];
TKBR_BrBluezonePoint = [0,0,0];
TKBR_BrBluezoneMarker = "";


TKBR_MapCenterPos =  [8500,8500];
TKBR_MapRadius = 8500;

TKBR_gameAreaRadius = 0;

TKBR_playersInitial = 0;
TKBR_playersRemaining = 0;
TKBR_groupsRemaining = 0;
TKBR_shortGame = false;

switch (toLower worldName) do {
	case "altis": {
		TKBR_minimumPlayers = 10;					// Players needed to start the game.
		TKBR_smallZoneMin = 20;					// Upper player threshold for the smaller blue zone system.


		TKBR_freeRoamTime = 6;   				// Total Free Roam time before the blue zone in announced, in minutes.
		if (TKBR_shortGame) then
		{
			TKBR_minimumPlayers = 5;					// Players needed to start the game.
			TKBR_gameAreaRadius = 2500;				// Radius, in meters, of the game area.
			TKBR_blueZoneSize = 1250;					// Initial starting radius, in multiples of 500m, of the blue zone. Should be less than half the size of the game area radius.
		} else {
			TKBR_gameAreaRadius = 4000;				// Radius, in meters, of the game area.
			TKBR_blueZoneSize = 2000;					// Initial starting radius, in multiples of 500m, of the blue zone. Should be less than half the size of the game area radius.
		};

		TKBR_blueZoneDelay = (5*60);				// Delay, in seconds, between blue zone reductions. (5*60) = 5 minutes. Must not be less than (2*60) / 2 minutes.
		TKBR_redZoneRadius = 300;					// Radius, in meters, of the red zone.

		TKBR_ospreyMaxSpawns = 4;

		TKBR_vehicleLimit = 75;
	};
};

0 call TKBR_fnc_pickupArea;
0 spawn TKBR_fnc_lobbyMessenger;

0 spawn TKBR_fnc_standbyPlayer;